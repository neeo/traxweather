package tk.neeo.traxweather;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class WidgetActivity extends AppWidgetProvider {
	private static final String ACTION_CLICK = "ACTION_CLICK";
	private static final String ACTION_UPDATE = "android.appwidget.action.APPWIDGET_UPDATE";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		//Log.i("neeo.tk", "onUpdate called");
		super.onUpdate(context, appWidgetManager, appWidgetIds);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName (), R.layout.weather_widget_layout);

        // When the icon on the widget is clicked, we send out a broadcast
        Intent intent = new Intent(context, WidgetActivity.class);
        intent.setAction(ACTION_CLICK);
        PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.weather_refresh, actionPendingIntent);

		for (int appWidgetId : appWidgetIds) {
			remoteViews.setViewVisibility(R.id.layout_aqi25, PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pm25_switch", true)?View.VISIBLE:View.GONE);
			remoteViews.setViewVisibility(R.id.layout_aqi10, PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pm10_switch", true)?View.VISIBLE:View.GONE);

			remoteViews.setViewVisibility(R.id.aqi25_desc, PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pm_verbose", true)?View.VISIBLE:View.GONE);
			remoteViews.setViewVisibility(R.id.aqi10_desc, PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pm_verbose", true)?View.VISIBLE:View.GONE);

			appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
		}

		// When we click the widget, we want to open our main activity.
        //Intent defineIntent2 = new Intent(context, MainActivity.class);
        //PendingIntent pendingIntent2 = PendingIntent.getActivity(context,0 /* no requestCode */, defineIntent2, 0 /* no flags */);
        //remoteViews.setOnClickPendingIntent(R.id.weather_widget, pendingIntent2);
  
        ComponentName cn = new ComponentName(context, WidgetActivity.class);
        AppWidgetManager.getInstance(context).updateAppWidget(cn, remoteViews);
    }

	@Override
	public void onReceive(Context context, Intent intent) {
		//Log.i("neeo.tk", "onReceive called with " + intent.getAction());
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.weather_widget_layout);

		switch (intent.getAction()) {
			case ACTION_UPDATE:
				//Log.i("neeo.tk", "ACTION_UPDATE");
				Bundle extras = intent.getExtras();
				if(extras != null) {
					AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
					ComponentName thisAppWidget = new ComponentName(context.getPackageName(), WidgetActivity.class.getName());
					int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);

					onUpdate(context, appWidgetManager, appWidgetIds);
				}
				break;

			case ACTION_CLICK:
				//Log.i("neeo.tk", "ACTION_CLICK");
				if (isOnline(context)) {
					new HttpAsyncTask(context).execute("http://neeo.tk/weather/json.php");
				}
				break;

			default:
				super.onReceive(context, intent);
				break;
		}

		ComponentName cn = new ComponentName(context, WidgetActivity.class);
        AppWidgetManager.getInstance(context).updateAppWidget(cn, remoteViews);
	}

	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();

		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
		private Context mContext;
		  
		public HttpAsyncTask(Context context){
		         mContext = context;
		  }

		public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
			Reader reader = null;
			reader = new InputStreamReader(stream, "UTF-8");
			char[] buffer = new char[len];
			reader.read(buffer);
			return new String(buffer);
		}

		private String downloadUrl(String myurl) throws IOException {
			InputStream is = null;
            int len = 512;

            try {
				URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);

				conn.connect();
				int response = conn.getResponseCode();
				is = conn.getInputStream();

				String contentAsString = readIt(is, len);
				return contentAsString;
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

		@Override
		protected String doInBackground(String... urls) {
			try {
				return downloadUrl(urls[0]);
			} catch (IOException e) {
				return "";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			//Intent uiIntent = new Intent(ACTION_UPDATE);
			//mContext.sendBroadcast(uiIntent);
	        	
			JSONObject json = null;
			try {
				json = new JSONObject(result);
			} catch (JSONException e) {
				Log.d("JSONObject", e.getLocalizedMessage());
			}

			RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(), R.layout.weather_widget_layout);

			if (json != null) {
				try {
					Bitmap myImg = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.wind_arrow);
					Matrix matrix = new Matrix();
					matrix.postRotate(Math.abs(180 - json.getInt("wind_dir_num")));
					Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(), matrix, true);

					remoteViews.setImageViewResource(R.id.img_weather_type, mContext.getResources().getIdentifier(json.getString("weather"), "drawable", mContext.getPackageName()));
	        		remoteViews.setTextViewText(R.id.weather_temp, new StringBuilder(json.getString("temp")).append("\u00B0").toString() + "C");
	        		remoteViews.setTextViewText(R.id.today, "Last updated: " + json.getString("last_update"));
	        		remoteViews.setTextViewText(R.id.sunrise_time, json.getString("sunrise"));
	        		remoteViews.setTextViewText(R.id.sunset_time, json.getString("sunset"));
	        		remoteViews.setTextViewText(R.id.weather_temp2, "(Felt: " + new StringBuilder(json.getString("temp_felt")).append("\u00B0").toString() + "C)");
	        		remoteViews.setTextViewText(R.id.weather_detail, "W: " + json.getString("wind_speed") + "m/s, H: " + json.getString("humidity") + "%");
					remoteViews.setTextViewText(R.id.aqi10, "PM10: " + new StringBuilder(json.getString("aqi10")).append("\u00B5").toString() + "g");
					remoteViews.setTextViewText(R.id.aqi10_desc, " (" + json.getString("aqi10_desc") + ")");
					remoteViews.setTextViewText(R.id.aqi25, "PM2.5: " + new StringBuilder(json.getString("aqi25")).append("\u00B5").toString() + "g");
					remoteViews.setTextViewText(R.id.aqi25_desc, " (" + json.getString("aqi25_desc") + ")");
					remoteViews.setImageViewBitmap(R.id.wind_dir, rotated);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

	        	ComponentName cn = new ComponentName(mContext, WidgetActivity.class);
	            AppWidgetManager.getInstance(mContext).updateAppWidget(cn, remoteViews);
		}
	}
} 
